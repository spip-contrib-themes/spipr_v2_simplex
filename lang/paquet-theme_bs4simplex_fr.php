<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

// Fichier produit par PlugOnet
// Module: paquet-theme_bs4simplex
// Langue: fr
// Date: 17-04-2020 16:50:04
// Items: 2

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// T
	'theme_bs4simplex_description' => 'Mini and minimalist',
	'theme_bs4simplex_slogan' => 'Mini and minimalist',
);
?>